# -*- coding: utf-8 -*-
from odoo import models, fields, api, exceptions
from datetime import date, datetime, time, timedelta
from odoo.fields import Date, Datetime
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT, float_compare
from odoo.exceptions import UserError


class LibraryBook(models.Model):
    _name = 'library.book'
    _description = 'Library Book'
    _rec_name = 'name'

    name = fields.Char(string='Name', required=True)
    issue_date = fields.Date(string='Issue Date')
    author_id = fields.Many2one("res.partner", 'Author')
    state = fields.Selection(string='State', selection=[('free', 'Free'), ('borrowed', 'Borrowed')], default='free')
    fees_borrowing = fields.Float('Fees Borrowing')

    @api.multi
    def button_borrow(self):
        for rec in self:
            rec.write({'state': 'borrowed'})






