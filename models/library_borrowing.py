# -*- coding: utf-8 -*-
from odoo import fields, models, api
from odoo.fields import Date, Datetime


class LibraryBorrowing(models.Model):
    _name = 'library.borrowing'
    _description = 'Library Borrowing'

    borrower_id = fields.Many2one("res.partner", 'Borrower')
    book_id = fields.Many2one('library.book', 'books', domain=[('state', '=', 'free')])
    author_id = fields.Many2one("res.partner", 'Author')
    date = fields.Date('Date')
    state = fields.Selection(string='State', selection=[('draft', 'Draft'), ('confirmed', 'Confirmed'), ('returned', 'Returned')],
                             default='draft')
    fees_borrowing = fields.Float('Fees Borrowing')

    @api.onchange('book_id')
    def onchange_book(self):
        self.author_id = self.book_id.author_id
        self.date = Date.today()
        self.fees_borrowing = self.book_id.fees_borrowing

    @api.multi
    def button_confirm(self):
        for rec in self:
            rec.state = 'confirmed'
            rec.book_id.state = 'borrowed'

    @api.multi
    def button_return(self):
        for rec in self:
            rec.state = 'returned'